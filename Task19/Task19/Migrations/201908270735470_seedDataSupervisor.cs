namespace Task19.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seedDataSupervisor : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Supervisors(Name) VALUES('Jack Anderson')");
            Sql("INSERT INTO Supervisors(Name) VALUES('Jimmy Karimi')");
            Sql("INSERT INTO Supervisors(Name) VALUES('Tommy Olson')");
        }
        
        public override void Down()
        {
            Sql("DELETE FROM Supervisors WHERE Name ='Jack Anderson'");
            Sql("DELETE FROM Supervisors WHERE Name ='Jimmy Karimi'");
            Sql("DELETE FROM Supervisors WHERE Name ='Tommy Olson'");
        }
    }
}
